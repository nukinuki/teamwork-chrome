/* Popup for Teamwork searcher */

var apikey = localStorage.getItem('apikey');
var twUrl = localStorage.getItem('twUrl');
var twUserId = localStorage.getItem('twUserId');
var instructionSeen = localStorage.getItem('instructionSeen');

var KEY_UP = 38;
var KEY_DOWN = 40;
var KEY_ENTER = 13;
var KEY_ESC = 27;

var KEY_A = 65; // For Credentials notebook

var CREDENTIALS_NOTEBOOK_NAME = "Доступы";

var searchMode = 'projects';
var projectId = false;
var projectName = '';

var xhr;

function twGet(url, data, callback, credentials){
	if(!credentials){
		credentials = {
			apikey: window.apikey,
			twUrl: window.twUrl
		};
	}

	// Abort previous request
	try {
	        xhr.onreadystatechange = null;
	        xhr.abort();
	} catch (e) {}

	xhr = $.ajax({
		type: "GET",
		url: credentials.twUrl + url,
		dataType: 'json',
		headers: {
			"Authorization": "Basic " + btoa(credentials.apikey + ':xxx')
		},
		data: data,
		success: callback,
		cache: false
	});
}

function twSearch(type, message, projectId){
	if(!type){
		throw 'Must specify type';
	}
	if(!message){
		return false;
	}
	projectId = projectId ? projectId : 0;
	twGet('/search.json', {
		'searchFor': type,
		'searchTerm': message,
		'projectId': projectId
	}, function(res){
		if(res.searchResult.projects){
			$('#result').remove();
			var output = $('<div id="result"></div>');
			output.append('<div class="result-title">Projects</div>');
			$.each(res.searchResult.projects, function(key, project){
				output.append('<div class="result-item" data-id="'+project.id+'"><a href="'+twUrl+'/projects/' + project.id + '/tasks" target="_blank">'+project.name+'</a></div>');
			});
			$('#page-search').append(output);
		} else if(res.searchResult.tasks){
			$('#result').remove();
			var output = $('<div id="result"></div>');
			output.append('<div class="result-title">Tasks for '+window.projectName+'</div>');
			$.each(res.searchResult.tasks, function(key, task){
				output.append('<div class="result-item" data-id="'+task.id+'"><a href="'+twUrl+'/tasks/' + task.id + '" target="_blank">'+task.name+'</a></div>');
			});
			$('#page-search').append(output);
		}
	});
}

function twGetTasklists(projectId){
	if(!projectId){
		return;
	}
	twGet('/projects/' + projectId + '/tasklists.json', {
	}, function(res){
		if(res.tasklists){
			$('#result').remove();
			var output = $('<div id="result"></div>');
			output.append('<div class="result-title">Tasklists in '+window.projectName+'</div>');
			$.each(res.tasklists, function(key, tasklist){
				output.append('<div class="result-item" data-id="'+tasklist.id+'"><a href="'+twUrl+'/tasklists/' + tasklist.id + '" target="_blank">'+tasklist.name+'</a></div>');
			});
			$('#page-search').append(output);
		}
	});
}

function twGetTodayTasks(){
	twGet('/tasks.json', {
		'filter': 'today',
		'sort': 'priority',
		'responsible-party-ids': window.twUserId
	}, function(res){
		if(res['todo-items']){
			$('#result').remove();
			var output = $('<div id="result"></div>');
			output.append('<div class="result-title">Today tasks</div>');
			$.each(res['todo-items'], function(key, task){
				output.append('<div class="result-item" data-id="'+task.id+'"><a href="'+twUrl+'/tasks/' + task.id + '" target="_blank">'+task.content+'<span class="project-name">'+task['project-name']+'</span></a></div>');
			});
			$('#page-search').append(output);
		}
	});
}

function twGetProjectCredentialsNotebook(projectId, callback){
	if(!projectId){
		throw 'Must specify projectId';
	}
	twGet('/projects/' + projectId + '/notebooks.json', {
	}, function(res){
		if(res.project && res.project.notebooks){
			var credentialsNotebook = res.project.notebooks.filter(function(notebook){
				return notebook.name == CREDENTIALS_NOTEBOOK_NAME;
			});
			if(credentialsNotebook.length > 0){
				callback(credentialsNotebook[0].id);
			}
		}
	});
}

function selectNextResult(){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	if($active.length == 0){
		$items.eq(0).addClass('active');
		return;
	}
	var index = $items.index($active);
	if(index + 1 < $items.length){
		$active.removeClass('active').next().addClass('active');
	}
	// Автоскролл
	$active = $items.filter('.active');
	var itemBottom = $active.offset().top + $active.height();
	var windowHeight = $(window).height();
	if(itemBottom > windowHeight + $(window).scrollTop() - 40){
		$(window).scrollTop(itemBottom - windowHeight + 40);
	}
}

function hasActive(){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	return $active.length != 0;
}

function selectPreviousResult(){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	if($active.length == 0){
		$items.eq(0).addClass('active');
		return;
	}
	var index = $items.index($active);
	if(index > 0){
		$active.removeClass('active').prev().addClass('active');
	}
	// Автоскролл
	$active = $items.filter('.active');
	var itemTop = $active.offset().top;
	if(itemTop < $(window).scrollTop() + 40){
		$(window).scrollTop(itemTop - 40);
	}
}

function activateResult(){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	if($active.length > 0){
		var url = $active.find('a').attr('href');
		//$active.find('a').click();
		window.open(url, '_blank');
		//console.log($active.find('a').length);
	}
}

function activateCredentials(e){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	if(searchMode == 'projects' && $active.length > 0){
		e.preventDefault();
		var projectId = $active.attr('data-id');
		if(projectId){
			twGetProjectCredentialsNotebook(projectId, function(notebookId){
				var notebookUrl = twUrl + '/notebooks/' + notebookId;
				window.open(notebookUrl, '_blank');
			});
		}
		return false;
	}
	return true;
}

function activateTaskSearchMode(){
	var $items = $('.result-item');
	var $active = $items.filter('.active');
	if($active.length > 0){
		window.projectId = $active.attr('data-id');
		window.projectName = $active.find('a').text();
		window.searchMode = 'tasks';
		$('#result').remove();
		$('#searchfield').val('').attr('placeholder', 'Search tasks...');
		twGetTasklists(window.projectId);
	}
}

function twCheckAuth(apikey, url, callbackOk, callbackError){
	if(!apikey || !url){
		callbackError();
	}
	var credentials = {};
	credentials.apikey = apikey;
	credentials.twUrl = url;
	twGet('/authenticate.json', {
			source: 'chrome-plugin'
   	}, function(res){
		if(res && res.STATUS == "OK"){
			callbackOk(res.account.userId);
		} else {
			callbackError();
		}
	}, credentials);
}

function initSearchPage(){
	console.log('Teamwork search init');
	var lastval = "";

	$('#page-search').show();
	$('#page-auth').hide();

	$('#searchfield').on('keyup', function(e){
		var message = $(this).val();
		if(message == lastval)
			return;
		lastval = message;
		if(message.length < 2){
			return;
		}
		switch(window.searchMode){
			case 'projects':
				twSearch('projects', message);
				break;
			case 'tasks':
				twSearch('tasks', message, window.projectId);
				break;
		}
		
	}).focus();

	twGetTodayTasks();

	$('#searchfield').on('keydown', function(e){
		console.log(e.which);
		//console.log(e.shiftKey);
		if(e.which == KEY_UP || e.which == KEY_DOWN || e.which == KEY_ENTER || e.which == KEY_A){
			if($('#result').length){
				switch(e.which) {
					case KEY_UP:
						selectPreviousResult();
						break;
					case KEY_DOWN:
						selectNextResult();
						break;
					case KEY_ENTER:
						if(e.shiftKey){
							activateTaskSearchMode();
						} else {
							if(hasActive()){
								activateResult();
							} else {
								var message = $(this).val();
								twSearch('tasks', message);
							}
						}
						break;
					case KEY_A:
						return activateCredentials(e);
						break;
				}
				e.stopPropagation();
				return false;
			}
		}
		if(e.which == KEY_ESC && window.searchMode == 'tasks'){
			window.searchMode = 'projects';
			window.projectId = false;
			window.projectName = '';
			$('#result').remove();
			$('#searchfield').val('').attr('placeholder', 'Search projects...');
			e.stopPropagation();
			return false;
		}
	});
}

function initAuthPage(){
	$('#page-auth').show();
	$('#page-search').hide();
	$('#apikey').focus();
	$('#authform').on('submit', function(e){
		$('#error').remove();
		e.stopPropagation();
		twCheckAuth($('#apikey').val(), $('#url').val(), function(id){
			window.apikey = $('#apikey').val();
			window.twUrl = $('#url').val();
			window.twUserId = id;
			localStorage.setItem('apikey', window.apikey);
			localStorage.setItem('twUrl', window.twUrl);
			localStorage.setItem('twUserId', window.twUserId);
			initSearchPage();
		}, function(){
			$('#authform').append('<div id="error">Ошибка авторизации</div>');
		});
		return false;
	});
}

$(function(){ 
	if(window.instructionSeen != $('#instruction').attr('data-version')){
		$('#instruction').show();
		$('#gotit').on('click', function(e){
			$('#instruction').hide();
			window.instructionSeen = $('#instruction').attr('data-version');
			localStorage.setItem('instructionSeen', window.instructionSeen);
		});
	}
	twCheckAuth(window.apikey, window.twUrl, function(id){
		window.twUserId = id;
		localStorage.setItem('twUserId', id);
		initSearchPage();
	}, function(){
		initAuthPage();
	});
});
